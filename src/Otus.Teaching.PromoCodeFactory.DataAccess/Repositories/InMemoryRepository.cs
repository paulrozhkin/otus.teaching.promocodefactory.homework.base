﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private Dictionary<Guid, T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToDictionary(x => x.Id, x => x);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.Values.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            Data.TryGetValue(id, out var entity);
            return Task.FromResult(entity);
        }

        public Task<T> Create(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity.Id, entity);
            return Task.FromResult(entity);
        }

        public Task Delete(Guid id)
        {
            Data.Remove(id);
            return Task.CompletedTask;
        }

        public Task<T> Update(T entity)
        {
            Data[entity.Id] = entity;
            return Task.FromResult(entity);
        }
    }
}