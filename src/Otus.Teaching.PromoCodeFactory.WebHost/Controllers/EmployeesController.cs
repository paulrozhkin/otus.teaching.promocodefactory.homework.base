﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository,
            IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync().ConfigureAwait(false);

            var employeesDtoList = _mapper.Map<IEnumerable<EmployeeShortResponse>>(employees);

            return employeesDtoList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id).ConfigureAwait(false);

            if (employee == null)
            {
                return Problem($"Employee with id {id} not found", $"GET api/v1/employees/{id}",
                    (int) HttpStatusCode.NotFound, "Can't find employee");
            }

            var employeeDto = _mapper.Map<EmployeeResponse>(employee);

            return employeeDto;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="createEmployeeDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(EmployeeCreateRequest createEmployeeDto)
        {
            var employeeRoles = await GetRolesByIds(createEmployeeDto.Roles).ConfigureAwait(false);

            if (!employeeRoles.Any())
            {
                return Problem("The employee must have at least one role. Role is missing or incorrect",
                    "POST api/v1/employees", (int) HttpStatusCode.BadRequest, "Can't create employee");
            }

            var employee = _mapper.Map<Employee>(createEmployeeDto);
            employee.Roles = employeeRoles.ToList();

            var newEmployee = await _employeeRepository.Create(employee);

            var employeeDto = _mapper.Map<EmployeeResponse>(newEmployee);

            return CreatedAtAction(nameof(CreateEmployee), new {id = employeeDto.Id}, employeeDto);
        }

        /// <summary>
        /// Удалить сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var existingEmployee = await _employeeRepository.GetByIdAsync(id).ConfigureAwait(false);

            if (existingEmployee == null)
            {
                return Problem($"Employee with id {id} not found", $"DELETE api/v1/employees/{id}",
                    (int) HttpStatusCode.NotFound, "Can't delete employee");
            }

            await _employeeRepository.Delete(id);

            return Ok();
        }

        /// <summary>
        /// Обновить, либо создать информацию о сотруднике
        /// </summary>
        /// <param name="id"></param>
        /// <param name="createEmployeeDto"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee(Guid id,
            EmployeeUpdateRequest createEmployeeDto)
        {
            var existingEmployee = await _employeeRepository.GetByIdAsync(id).ConfigureAwait(false);
            if (existingEmployee == null)
            {
                return Problem($"Employee with id {id} not found", $"PUT api/v1/employees/{id}",
                    (int) HttpStatusCode.NotFound, "Can't update employee");
            }

            var employeeRoles = await GetRolesByIds(createEmployeeDto.Roles).ConfigureAwait(false);

            if (!employeeRoles.Any())
            {
                return Problem("The employee must have at least one role. Role is missing or incorrect",
                    $"PUT api/v1/employees/{id}", (int) HttpStatusCode.BadRequest, "Can't update employee");
            }

            var employee = _mapper.Map<Employee>(createEmployeeDto);
            employee.Roles = employeeRoles.ToList();
            employee.Id = id;

            var newEmployee = await _employeeRepository.Update(employee);

            var employeeDto = _mapper.Map<EmployeeResponse>(newEmployee);

            return employeeDto;
        }

        private async Task<List<Role>> GetRolesByIds(IEnumerable<Guid> ids)
        {
            var getRolesTasks = ids.Select(x => _roleRepository.GetByIdAsync(x));
            var employeeRoles = (await Task.WhenAll(getRolesTasks).ConfigureAwait(false)).Where(x => x != null)
                .ToList();
            return employeeRoles;
        }
    }
}