﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRoleItemResponse
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}