﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure.Profiles
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            // DTOs

            // Employee controller
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Role, EmployeeRoleItemResponse>();
            CreateMap<EmployeeCreateRequest, Employee>()
                .ForMember(x => x.Roles, opt => opt.Ignore()); // Set from another entity

            CreateMap<EmployeeUpdateRequest, Employee>()
                .ForMember(x => x.Roles, opt => opt.Ignore()); // Set from another entity

            // Role controller
            CreateMap<Role, RoleItemResponse>();
        }
    }
}